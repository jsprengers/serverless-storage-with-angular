import {Injectable} from "@angular/core";
import {Address} from "./model";

@Injectable()
export class FileService {

  private model: Array<Address> = []

  newAddress(): Address {
    return this.storeAddress(new Address('New name'))
  }

  getModel(): Array<Address> {
    return this.model;
  }

  private storeAddress(address: Address) {
    this.model.push(address)
    return address
  }

  removeAddress(index: number) {
    this.model.splice(index, 1);
    this.saveToLocalStorage()
  }

  download() {
    let file: File = new File([JSON.stringify(this.model)], "data.json", {type: 'application/octet-stream'})
    var url = window.URL.createObjectURL(file);
    window.open(url);
  }

  upload(file: File, onSuccess: (Array, Address) => void) {
    file.text().then(txt => {
      let m = this.deserialize(txt)
      if (m != null) {
        this.model = m
        this.saveToLocalStorage()
        onSuccess.apply(this.model)
      }
    })
  }

  loadFromLocalStorage() {
    let stored = localStorage.getItem("serverless-storage-demo")
    this.model = this.deserialize(stored)
  }

  private deserialize(content: string): Array<Address> | null {
    try {
      let rows = JSON.parse(content).filter(a => !this.empty(a)) as Array<any>
      return rows.map(a => new Address(a.name, a.street, a.town))
    } catch (e) {
      console.error("Unable to deserialize stored object to valid UserData. Using fresh item instead: " + e)
      return []
    }
  }

  saveToLocalStorage() {
    if (!this.empty(this.model)) {
      let serialized = JSON.stringify(this.model!)
      if (!this.empty(serialized)) {
        localStorage.setItem("serverless-storage-demo", serialized)
      }
    }
  }

  private empty(x: any): boolean {
    return x === null || x === undefined
  }

}
