export class Address {
  constructor(name: string, street?: string, town?: string) {
    this.name = name;
    this.street = street;
    this.town = town;
  }
  name: string
  street: string
  town: string
}
