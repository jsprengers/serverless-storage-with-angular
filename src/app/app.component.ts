import {Component, OnInit} from '@angular/core';
import {FileService} from "./file-service";
import {Address} from "./model";
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {debounceTime} from "rxjs/operators";

@Component({
  selector: 'app-root',
  template: `
    <div class="main-ui-block">
      <h2>Serverless-storage sample</h2>
      <div class="form-group">
        <small class="form-text text-muted">
          Changes to this table will be automatically saved to local storage and persist between browser sessions.
        </small>
        <div [formGroup]="mainForm">
          <div formArrayName="addresses">
            <table class="table table-bordered table-hover table-condensed col-sm-8">
              <thead>
              <tr>
                <th scope="col">Name</th>
                <th scope="col">Street</th>
                <th scope="col">Town</th>
                <th scope="col"></th>
              </tr>
              </thead>
              <tbody>
              <tr *ngFor="let address of addresses.controls; let i=index" [formGroupName]="i">
                <!-- The repeated alias template -->
                <td><input formControlName="name"></td>
                <td><input formControlName="street"></td>
                <td><input formControlName="town"></td>
                <td>
                  <button class="btn-danger btn-sm" (click)="removeAddress(i)">Remove</button>
                </td>
              </tr>
              </tbody>
            </table>

            <button class="btn btn-primary btn-sm mainbutton" (click)="addAddress()">Add address</button>
          </div>
          <h3>File management</h3>
          <div class="form-group">
            <label for="download">Download data as a JSON file</label>
            <DIV>
              <button type="button" class="btn btn-secondary btn-sm" id="download"
                      (click)="download()">Download
              </button>
            </DIV>
            <small id="defaultRehearsalSpacingHelp" class="form-text text-muted">
              This serializes the above table to a standard JSON structure.
            </small>
          </div>

          <div class="form-group">
            <label for="Upload">Replace table with JSON file</label>
            <div>
            <input type="file" (change)="selectFile($event)" #fileUpload id="fileUpload" name="fileUpload" multiple=""
                   accept="*/*"/>
            <button class="btn btn-secondary btn-sm" [disabled]="!selectedFiles"
                    (click)="upload()">Upload
            </button>
            </div>
            <small id="defaultRehearsalSpacingHelp" class="form-text text-muted">
              Upload a valid JSON file to replace the data table content.
            </small>
          </div>

        </div>
      </div>
    </div>`
})
export class AppComponent implements OnInit {

  title = 'serverless-upload';
  selectedFiles: FileList;
  currentFile: File;
  mainForm = this.fb.group({
    addresses: this.fb.array([])
  });

  constructor(private fileService: FileService,
              private fb: FormBuilder) {
  }

  ngOnInit(): void {
    this.fileService.loadFromLocalStorage()
    this.loadForm()
  }

  get addresses(): FormArray {
    return this.mainForm.get('addresses') as FormArray;
  }

  addAddress() {
    this.addresses.push(this.createFormGroup(this.fileService.newAddress()));
  }

  loadForm() {
    this.addresses.clear()
    this.fileService.getModel().forEach(a => this.addresses.push(this.createFormGroup(a)))
  }

  private createFormGroup(address: Address): FormGroup {
    return this.fb.group({
      name: this.createFormControl(address, address.name, (a, x) => a.name = x),
      street: this.createFormControl(address, address.street, (a, x) => a.street = x),
      town: this.createFormControl(address, address.town, (a, x) => a.town = x)
    })
  }

  private createFormControl(address: Address, value: string, updater: (Address, string) => void): FormControl {
    let control = new FormControl(value, Validators.required)
    control.valueChanges.pipe(debounceTime(1000)).subscribe(x  => {
      updater.apply(updater, [address, x])
      if (this.mainForm.valid)
        this.fileService.saveToLocalStorage()
    })
    return control;
  }

  removeAddress(index: number) {
    this.fileService.removeAddress(index)
    this.addresses.removeAt(index)
  }

  selectFile(event): void {
    this.selectedFiles = event.target.files;
  }

  download() {
    this.fileService.download()
  }

  upload(): void {
    this.currentFile = this.selectedFiles.item(0)!;
    this.fileService.upload(this.currentFile, () => this.loadForm())
  }

}
